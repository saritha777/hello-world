import { Module } from "@nestjs/common";
import { programsController } from "./programs.controller";
import { programsService } from "./programs.service";

@Module({
    controllers: [programsController],
    providers: [ programsService]
})

export class programsModule { }