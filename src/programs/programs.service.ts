import { Injectable } from "@nestjs/common";

@Injectable()
export class programsService {

    //factorial
    factorial(num:number):number {
        let factorial:number = 1; 
        while(num >=1) { 
        factorial = factorial * num; 
        num--; 
         } 
        return factorial;
    }

    //fibonacci
    fib(num: number):number[] {
        let fibonacci: number[] = [0, 1];
        for (let i: number = 2; i < num; i++) {
          fibonacci[i] = fibonacci[i - 2] + fibonacci[i - 1];
        }
        return fibonacci;
      }

      //insert
      getArr(): string[]{
        let names: string[] = ['saritha','chinna','satish','teja']
        names.splice(2,0,'pandu');
        return names;
    }

    //map
    getMap(): string[]{
        let Names:string[]= ['saritha','chinna','satish','teja'];
        let lastName:string = 'reddy'
        let newArray = Names.map((name) => name + lastName);
        return newArray;
     }

     //replace
     replaceStr(str: string, change: string, replace: string):string {
        let newStr: string = str.replace(change, replace);
        return newStr;
    }

    //reverse
    revArr(arr:string): string[]{
        let newArr:string[] = new Array;
        for(let i = arr.length-1; i >= 0; i--) {
            newArr.push(arr[i]);
        }
         return newArr;
    }

}