import { Controller, Get, Param } from "@nestjs/common";
import { programsService } from "./programs.service";

@Controller("programs")
export class programsController {
    
    constructor(private readonly programsService: programsService) {}

    //factorial
    @Get("fact/:num")
    fact(@Param('num') num: number): number {
        return this.programsService.factorial(num);
    }

    //fibonacci
    @Get("/fib/:num")
    fib(@Param('num') num: number): number[] {
        return this.programsService.fib(num);
    }

    //insert
    @Get("/insert")
    Arr(): string[] {
        return this.programsService.getArr();
    }

    //map
    @Get("/map")
    arr(): string[] {
        return this.programsService.getMap();
    }

    //replace
    @Get("/replace/:str/:changeValue/:replaceValue")
    replace(@Param('str') str:string, @Param('changeValue') change: string, @Param('replaceValue') replace:string): string {
        return this.programsService.replaceStr(str, change, replace);
    }

    //reverse
    @Get("/reverse/:str")
    reverse(@Param('str') str: string): string[] {
        return this.programsService.revArr(str);
    }
}