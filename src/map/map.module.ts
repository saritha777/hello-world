import { Module } from "@nestjs/common";
import { mapController } from "./map.controller";
import { mapService } from "./map.service";

@Module({
    controllers: [mapController],
    providers: [mapService]
})
export class mapModule { }