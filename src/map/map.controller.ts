import { Controller, Get } from "@nestjs/common";
import { mapService } from "./map.service";

@Controller("map")
export class mapController {
    constructor(private readonly mapService: mapService) {}
    @Get()
    arr(): string[] {
        return this.mapService.getArr();
    }
}