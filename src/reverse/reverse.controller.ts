import { Controller, Get } from "@nestjs/common";
import { reverseService } from "./reverse.service";

@Controller("reverse")
export class reverseController {
    constructor(private readonly reverseService: reverseService) {}
    @Get()
    reverse(): string[] {
        return this.reverseService.revArr();
    }
}