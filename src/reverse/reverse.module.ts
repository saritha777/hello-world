import { Module } from "@nestjs/common";
import { reverseController } from "./reverse.controller";
import { reverseService } from "./reverse.service";

@Module({
    controllers: [reverseController],
    providers: [reverseService]
})
export class reverseModule { }