import { Module } from "@nestjs/common";
import { fibController } from "./fibonacci.controller";
import { fibService } from "./fibonacci.service";

@Module({
    controllers: [fibController],
    providers: [fibService]
})
export class fibModule { }