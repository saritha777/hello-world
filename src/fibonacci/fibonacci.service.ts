import { Injectable } from "@nestjs/common";
@Injectable()
export class fibService{
    fib(num: number):number[] {
        let fibonacci: number[] = [0, 1];
        for (let i: number = 2; i < num; i++) {
          fibonacci[i] = fibonacci[i - 2] + fibonacci[i - 1];
        }
        return fibonacci;
      }
}