import { Controller, Get, Param } from "@nestjs/common";
import { fibService } from "./fibonacci.service";

@Controller("fib")
export class fibController {
    constructor(private readonly fibonacciService: fibService) {}
    @Get("/:num")
    fib(@Param('num') num: number): number[] {
        return this.fibonacciService.fib(num);
    }
}