import { Controller, Get } from "@nestjs/common";
import { insertService } from "./insert.service";

@Controller("add")
export class insertController {
    constructor(private readonly insertService: insertService) {}
    @Get()
    Arr(): string[] {
        return this.insertService.getArr();
    }
}