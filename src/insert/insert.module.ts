import { Module } from "@nestjs/common";
import { insertController } from "./insert.controller";
import { insertService } from "./insert.service";

@Module({
    controllers: [insertController],
    providers: [insertService]
})
export class insertModule { }