import { Injectable } from "@nestjs/common";

@Injectable()
export class replaceService{
    replaceStr():string {
        let str: string = 'welcome to typescript training';
        let newStr: string = str.replace('typescript', 'nestjs');
        return newStr;
    }
}