import { Module } from "@nestjs/common";
import { replaceController } from "./replace.controller";
import { replaceService } from "./replace.service";

@Module({
    controllers: [replaceController],
    providers: [replaceService]
})
export class replaceModule { }