import { Controller, Get } from "@nestjs/common";
import { replaceService } from "./replace.service";

@Controller("replace")
export class replaceController {
    constructor(private readonly replaceService: replaceService) {}
    @Get()
    replace(): string {
        return this.replaceService.replaceStr();
    }
}