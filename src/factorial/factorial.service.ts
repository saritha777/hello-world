import { Injectable } from "@nestjs/common";
@Injectable()
export class factService{
    factorial(num:number):number {
        let factorial:number = 1; 
        while(num >=1) { 
        factorial = factorial * num; 
        num--; 
         } 
        return factorial;
    }

}