import { Controller, Get, Param } from "@nestjs/common";
import { factService } from "./factorial.service";

@Controller("fact")
export class factController {
    constructor(private readonly factService: factService) {}
    @Get("/:num")
    fact(@Param('num') num: number): number {
        return this.factService.factorial(num);
    }
}