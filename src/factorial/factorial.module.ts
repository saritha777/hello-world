import { Module } from "@nestjs/common";
import { factController } from "./factorial.controller";
import { factService } from "./factorial.service";

@Module({
    controllers: [factController],
    providers: [factService]
})
export class factModule { }