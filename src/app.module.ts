import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { factModule } from './factorial/factorial.module';
import { fibModule } from './fibonacci/fibonacci.module';
import { insertModule } from './insert/insert.module';
import { mapModule } from './map/map.module';
import { programsModule } from './programs/programs.module';
import { replaceModule } from './replace/replace.module';
import { reverseModule } from './reverse/reverse.module';

@Module({
  imports: [factModule, fibModule, replaceModule, reverseModule, insertModule, mapModule, programsModule],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
